let liveList = new Array();
var guestUserIdLive = "user" + Math.floor(Math.random() * 1000) + Date.now();
var publicIp;

$.getJSON('http://www.geoplugin.net/json.gp', function (data) {

    publicIp = data['geoplugin_request'];
    console.log(publicIp)
});
$(function () {
    var webUserId = "user" + Math.floor(Math.random() * 1000) + Date.now();
    var channelIdForLiveList = "guest-c1772e2a-88e6-45cb-ae5d-e79ae1e01471";
    var lastPostId;
    var lastPostDate;
    var currentChannelId = $("#channel_id_for_stream").val();
    var isLastPagePost = "false";
    var baseUrl = readBaseURL();
    $("body").on(
        "click", 
        ".btn-download-moodapp, .getMoodAppDesktop, .desktop-view .download-to-comment,\
        [data-e2e='share-icon'], [data-e2e='live-room-follow']", 
        onClickDownloadMoodBtn
    );
    
    $("body").on(
        "click",
        ".mobile-view .mobile-footer .upload-l, .mobile-view .mobile-footer .account-l,\
        #videos-container-mobile, .mobile-view .open-mood-app-btn, .follow-button-mobile",
        onClickDownloadMoodBtn
    );
    $("body").on("click", ".btn-close-download-moodapp-modal", onClickCancelDownloadMoodBtn);
    $downloadMoodappModalMobile = $("#downloadMoodappModalMobile");
    $downloadMoodappModalDesktop = $("#downloadMoodAppModalDesktop");

    var isMobile = isMobileDevice();
    function isMobileDevice() {
        return /Android|iPhone|iPad|iPod|webOS|BlackBerry|IEMobile|Opera Mini/i.test(
            navigator.userAgent
        );
    }

    function onClickDownloadMoodBtn() {
        if (isMobile) {
            $downloadMoodappModalMobile.modal("show");
        } else {
            $downloadMoodappModalDesktop.modal("show");
        }
    }
    function onClickCancelDownloadMoodBtn() {
        if (isMobile) {
            $downloadMoodappModalMobile.modal("hide");
        } else {
            $downloadMoodappModalDesktop.modal("hide");
        }
    }
    if (isMobile) {
        let postList = new Array();
        var isLastPagePostP = "false";
        var lastPostIdP;
        var lastPostDateP;
        document.querySelector(".mobile-view").style.display = "block";
        hideLoaderMobile();

        function hideLoaderMobile() {
            $(".loader").css("display", "none");
            $(".contentMobile").css("display", "block");
        }

        loadFirstLiveMobile();
        function loadFirstLiveMobile() {
            var channel_link_with_character = $("#channel_link_for_Stream").val();
            var channel_link = channel_link_with_character.substring(1);
            loadSingleLiveAjaxCallMobile(channel_link);
        }

        function updateMainChannelLinkMobile(channel_link) {
            var channelUrl = baseUrl + "/" + "@" + channel_link;
            $(".liveContainerMobile .channel-url").attr("href", channelUrl);
            $(".liveContainerMobile .channel-link-l").attr("href", channelUrl);
        }

        function loadSingleLiveAjaxCallMobile(channelLink) {
            https: $.ajax({
                url:
                    `${API_CONTEXT}` +
                    "/micro-service/live/stream-info?link=" +
                    channelLink +
                    "&activeChannelId=" +
                    channelIdForLiveList +
                    "&ipAddress=" + publicIp,
                type: "GET",
                headers: { token: "12345678", userId: guestUserIdLive },

                success: function (response) {
                    loadSingleLiveMobile(response, channelLink);
                },
                error: function (response) { },
            });
        }
        window.loadSingleLiveAjaxCallMobile = loadSingleLiveAjaxCallMobile;

        function loadSingleLiveMobile(response, channelLink) {
            var data;
            if (response.status == 200) {
                data = response.data;
            }
            $(".normalLiveMobileContainer").empty();
            $(".guestLiveMobileContainer").empty();
            $(".hostLiveMobileContainer").empty();
            $(".cohostLiveMobileContainer").empty();
            updateMainChannelLinkMobile(channelLink);
            if (!response.data) {
                $(".liveContainerMobile .message-box").text("Live Not Available Now");
                $(".mobile-view .normalLiveMobileContainer").css({
                    "background-repeat": "no-repeat",
                    "background-size": "cover",
                    "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                });
                $(".main-live-profile-frame-mobile").attr('src', '');
                $(".main-live-profile-frame-mobile").css("display", "none");
                // liveList = liveList.filter(function (item) {
                //     return item.channel.link !== channelLink;
                // });
                $(".mobile-view .guestLiveMobileContainer").css("display", "none");
                $(".mobile-view .cohostMobileContainer").css("display", "none");
                $(".mobile-view .normalLiveMobileContainer").css("display", "block");
                $(".liveContainerMobile .message-box").text("Live Not Available Now");
            }
            else {

                const channel = response.data.channel;
                const post = response.data.post;
                lastPostId = post.id;
                lastPostDate = post.timestamp;
                $(".liveContainerMobile .message-box").text("");
                $(".liveContainerMobile .channel-name").text(channel.name);
                $(".liveContainerMobile .channel-link-input").val(channel.link);

                if (channel.profileImage != "null") {
                    $('.liveContainerMobile .profile-image').attr('src', channel.profileImage);
                }
                else {
                    $('.liveContainerMobile .profile-image').attr('src', '/assets/img/MOOD_DEFAULT.png');
                }
                if (channel.frame != null) {
                    $(".liveContainerMobile .main-live-profile-frame-mobile").attr('src', channel.frame);
                    $(".liveContainerMobile .main-live-profile-frame-mobile").css("display", "block");
                }
                else {
                    $(".liveContainerMobile .main-live-profile-frame-mobile").attr('src', '');
                    $(".liveContainerMobile .main-live-profile-frame-mobile").css("display", "none");
                }

                // $(".liveContainer").css("display", "flex");
                if (post.postType == "normal") {
                    $(".mobile-view .guestLiveMobileContainer").css("display", "none");
                    $(".mobile-view .cohostMobileContainer").css("display", "none");
                    $(".mobile-view .normalLiveMobileContainer").css("display", "block");

                    if (channel.profileImage != "null") {
                        $(".mobile-view .normalLiveMobileContainer").css({
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".mobile-view .normalLiveMobileContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }

                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        cLink: channel.link,
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();
                }
                else if (post.postType == "guest") {
                    var guestListDiv = $("<div>").addClass("guestListContainerMobile");
                    $("#liveStreamGuestContainerMobile").append(guestListDiv);
                    const guestListContainer = document.querySelector(".guestLiveMobileContainer .guestListContainerMobile");
                    const existingGuestDivs = guestListContainer.querySelectorAll(".guestDiv");
                    existingGuestDivs.forEach((div) => {
                        div.remove();
                    });
                    $(".mobile-view .normalLiveMobileContainer").css("display", "none");
                    $(".mobile-view .cohostMobileContainer").css("display", "none");
                    $(".mobile-view .guestLiveMobileContainer").css("display", "block");

                    if (channel.profileImage != "null") {
                        $(".mobile-view .guestLiveMobileContainer").css({
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".mobile-view .guestLiveMobileContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    createGuestListDivsMobile(post.guest);

                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        guestList: post.guest,
                        cLink: channel.link,
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();
                }
                else if (
                    post.postType == "co-host" ||
                    post.postType == "pk"
                ) {
                    $(".mobile-view .normalLiveMobileContainer").css("display", "none");
                    $(".mobile-view .guestLiveMobileContainer").css("display", "none");
                    $(".mobile-view .cohostMobileContainer").css("display", "flex");

                    if (channel.profileImage != "null") {
                        $(".mobile-view .hostLiveMobileContainer").css({
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".mobile-view .hostLiveMobileContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    if (post.cohost.imageUrl != "null") {
                        $(".mobile-view .cohostLiveMobileContainer").css({
                            "background-image": "url(" + post.cohost.imageUrl + ")",
                        });
                    }
                    else {
                        $(".mobile-view .cohostLiveMobileContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        cLink: channel.link,
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();

                }

            }

        }

        function createGuestListDivsMobile(guestArray) {
            const guestListContainerMobile = document.querySelector(".guestLiveMobileContainer .guestListContainerMobile");
            guestListContainerMobile.style.gridTemplateColumns = `repeat(3, 1fr)`; // Set grid columns
            for (let i = guestArray.length - 1; i >= 0; i--) {
                const guest = guestArray[i];
                const div = document.createElement("div");
        
                div.id = "guest" + guest.channelId;
                div.className = "guestDiv";
                // Set inline styles to display from right to left
                div.style.gridColumn = `${3 - (i % 3)}`; // Adjust the starting column index
                guestListContainerMobile.appendChild(div);
            }
        }
        
        

        getPostsMobile();
        async function getPostsMobile() {
            if (isLastPagePostP == "false") {
                var url;
                url =
                    `${API_CONTEXT}` +
                    "/micro-service/channel/post?c_id=" +
                    currentChannelId +
                    "&country=QA&lastPostId=" +
                    lastPostIdP +
                    "&lastPostedDate=" +
                    lastPostDateP +
                    "&pageSize=15&ipAddress=" + publicIp;
                await $.ajax({
                    url: url,
                    type: "GET",
                    headers: { token: "12345678", userId: guestUserIdLive },
                    success: function (response) {
                        if (response.data) {
                            if (response.data.posts.length > 0) {
                                if (response.data.pagination.isLastPage == "true") {
                                    isLastPagePostP = "true"; // Fix: Change '==' to '='
                                }
                                lastPostIdP = response.data.pagination.lastPostedId;
                                lastPostDateP = response.data.pagination.lastPostedDate;
                                var datas = response.data.posts;
                                datas.forEach((data) => {
                                    const postCheckId = data.id;
                                    let isIdInArray = false;
                                    for (let i = 0; i < postList.length; i++) {
                                        if (postList[i].id == postCheckId) {
                                            isIdInArray = true;
                                            break;
                                        }
                                    }
                                    if (isIdInArray == false) {
                                        postList.push(data);
                                        populatePostsMobile(data);
                                    }
                                });
                            } else {
                                noPostHandlerMobile();
                            }
                        }
                        else {
                            console.log('error');
                        }
                    },
                    error: function (response) {
                        console.log(response);
                    },
                });
            } else {
                console.log('all posts loaded');
            }
        }
        function noPostHandlerMobile() {
            $(".mobile-view .stories-video-title").css("display", "none");
            $(".mobile-view .div-footer-guide-wrapper ").css("margin-top", "30px");
        }

        function populatePostsMobile(item) {
            const storiesContainer = document.getElementById('videos-container-mobile');
            const post = item;
            const storyElement = document.createElement('div');
            storyElement.className = 'videoContainerParent';

            let htmlContent = '';
            htmlContent += `<div data-target="${post.id}" class="prime-video" style="background-image: url('${post.thumbnail}');">`;
            htmlContent += '<div class="view-count">';
            htmlContent += '<img src="/assets/img/website-viewer.svg" width="30" height="30" alt="View Count">';
            htmlContent += '<span class="storyVideoViewCount">' + post.viewCount + '</span>';
            htmlContent += '</div>';
            htmlContent += '</div>';

            storyElement.innerHTML = htmlContent;
            storiesContainer.appendChild(storyElement);
        }

        window.addEventListener("beforeunload", function (e) {
            // e.preventDefault();
            // e.returnValue = ""; // This is necessary for Chrome
            rtcClient.leave();
        });

        window.addEventListener("unload", function (e) {
            rtcClient.leave();
        });
        window.addEventListener("pagehide", function () {
            rtcClient.leave();
        });

        document.addEventListener("visibilitychange", function () {
            if (document.visibilityState === "hidden") {
                rtcClient.leave();
            } else if (document.visibilityState === "visible") {
                rtcClient.join();
            }
        });
    }

    else {
        document.querySelector(".desktop-view").style.display = "block";
        hideLoader();
        $(".liveContainer").on("click", ".channel-header", redirectToChannel);
        var baseUrl = readBaseURL();
        $('.main-logo-link').attr('href', baseUrl);
        var chatOpen = true;
        $(".comment-section").on("click", ".chat-close-button", closeChatRoom);
        $(".channel-info-main-div").on("click", ".chat-open-button", openChatRoom);

        loadFirstLive();
        function loadFirstLive() {
            var channel_link_with_character = $("#channel_link_for_Stream").val();
            var channel_link = channel_link_with_character.substring(1);
            loadSingleLiveAjaxCall(channel_link);
        }

        function updateMainChannelLink(channel_link) {
            var channelUrl = baseUrl + "/" + "@" + channel_link;
            $(".channel-info-main-div .channel-url").attr("href", channelUrl);
            $(".channel-info-main-div .channel-link-l").attr("href", channelUrl);
        }

        function loadSingleLiveAjaxCall(channelLink) {

            https: $.ajax({
                url:
                    `${API_CONTEXT}` +
                    "/micro-service/live/stream-info?link=" +
                    channelLink +
                    "&activeChannelId=" +
                    channelIdForLiveList +
                    "&ipAddress=" + publicIp,
                type: "GET",
                headers: { token: "12345678", userId: guestUserIdLive },

                success: function (response) {
                    loadSingleLive(response, channelLink);
                },
                error: function (response) { },
            });
        }
        window.loadSingleLiveAjaxCall = loadSingleLiveAjaxCall;

        function loadSingleLive(response, channelLink) {
            var data;
            if (response.status == 200) {
                data = response.data;
            }
            $(".normalLiveDesktopContainer").empty();
            $(".guestLiveDesktopContainer").empty();
            $(".hostLiveDesktopContainer").empty();
            $(".cohostLiveDesktopContainer").empty();
            updateMainChannelLink(channelLink);
            if (!response.data) {
                $(".liveContainer .message-box").text("Live Not Available Now");
                $(".desktop-view .normalLiveDesktopContainer").css({
                    "background-repeat": "no-repeat",
                    "background-size": "cover",
                    "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                });
                $(".desktop-view .guestLiveDesktopContainer").css({
                    "background-repeat": "no-repeat",
                    "background-size": "cover",
                    "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                });
                $(".channel-info-main-div .main-live-profile-frame").attr('src', '');
                $(".channel-info-main-div .main-live-profile-frame").css("display", "none");
                // liveList = liveList.filter(function (item) {
                //     return item.channel.link !== channelLink;
                // });
                $(".liveContainer").css("display", "flex");
                $(".desktop-view .guestLiveDesktopContainer").css("display", "none");
                $(".desktop-view .cohostDesktopContainer").css("display", "none");
                $(".desktop-view .normalLiveDesktopContainer").css("display", "block");
            }
            else {

                const channel = response.data.channel;
                const post = response.data.post;
                lastPostId = post.id;
                lastPostDate = post.timestamp;
                $(".liveContainer .message-box").text("");
                $(".channel-info-main-div .channel-name").text(channel.name);
                $(".liveContainer .channel-link-input").val(channel.link);

                if (channel.profileImage != "null") {
                    $('.channel-info-main-div .profile-image').attr('src', channel.profileImage);
                }
                else {
                    $('.channel-info-main-div .profile-image').attr('src', '/assets/img/MOOD_DEFAULT.png');
                }
                if (channel.frame != null) {
                    $(".channel-info-main-div .main-live-profile-frame").attr('src', channel.frame);
                    $(".channel-info-main-div .main-live-profile-frame").css("display", "block");
                }
                else {
                    $(".channel-info-main-div .main-live-profile-frame").attr('src', '');
                    $(".channel-info-main-div .main-live-profile-frame").css("display", "none");
                }

                $(".liveContainer").css("display", "flex");
                //Normal Live Begins
                if (post.postType == "normal") {
                    $(".desktop-view .guestLiveDesktopContainer").css("display", "none");
                    $(".desktop-view .cohostDesktopContainer").css("display", "none");
                    $(".desktop-view .normalLiveDesktopContainer").css("display", "block");
                    if (channel.profileImage != "null") {
                        $(".desktop-view .normalLiveDesktopContainer").css({
                            "background-repeat": "no-repeat",
                            "background-size": "cover",
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".desktop-view .normalLiveDesktopContainer").css({
                            "background-repeat": "no-repeat",
                            "background-size": "cover",
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }

                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        cLink: channel.link,
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();

                }
                //Normal Live Ends

                //Guest Live Begins
                else if (post.postType == "guest") {
                    var guestListDiv = $("<div>").addClass("guestListContainer");
                    $("#liveStreamGuestContainer").append(guestListDiv);
                    const guestListContainer = document.querySelector(".guestLiveDesktopContainer .guestListContainer");
                    const existingGuestDivs = guestListContainer.querySelectorAll(".guestDiv");
                    existingGuestDivs.forEach((div) => {
                        div.remove();
                    });
                    $(".desktop-view .normalLiveDesktopContainer").css("display", "none");
                    $(".desktop-view .cohostDesktopContainer").css("display", "none");
                    $(".desktop-view .guestLiveDesktopContainer").css("display", "block");
                    if (channel.profileImage != "null") {
                        $(".desktop-view .guestLiveDesktopContainer").css({
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".desktop-view .guestLiveDesktopContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    createGuestListDivs(post.guest);

                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        cLink: channel.link,
                        guestList: post.guest
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();
                }
                //Guest Live Ends

                //Cohost Live Starts
                else if (
                    post.postType == "co-host" ||
                    post.postType == "pk"
                ) {

                    $(".desktop-view .normalLiveDesktopContainer").css("display", "none");
                    $(".desktop-view .guestLiveDesktopContainer").css("display", "none");
                    $(".desktop-view .cohostDesktopContainer").css("display", "flex");

                    if (channel.profileImage != "null") {
                        $(".desktop-view .hostLiveDesktopContainer").css({
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".desktop-view .hostLiveDesktopContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    if (post.cohost.imageUrl != "null") {
                        $(".desktop-view .cohostLiveDesktopContainer").css({
                            "background-image": "url(" + post.cohost.imageUrl + ")",
                        });
                    }
                    else {
                        $(".desktop-view .cohostLiveDesktopContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        cLink: channel.link,
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();
                }
                //Co host live ends
            }

        }

        function createGuestListDivs(guestArray) {
            const guestListContainer = document.querySelector(".guestLiveDesktopContainer .guestListContainer");
            guestListContainer.style.gridTemplateColumns = `repeat(3, 1fr)`; // Set grid columns
            for (let i = guestArray.length - 1; i >= 0; i--) {
                const guest = guestArray[i];
                const div = document.createElement("div");
        
                div.id = "guest" + guest.channelId;
                div.className = "guestDiv";
                // Set inline styles to display from right to left
                div.style.gridColumn = `${3 - (i % 3)}`; // Adjust the starting column index
                guestListContainer.appendChild(div);
            }
        }
        

        getLiveStreamsDesktop();
        async function getLiveStreamsDesktop() {
            if (isLastPagePost == "false") {
                var url;
                url =
                    `${API_CONTEXT}` +
                    "/micro-service/live/stream?channelId=" +
                    channelIdForLiveList +
                    "&lastPostedId=" +
                    lastPostId +
                    "&lastPostedDate=" +
                    lastPostDate +
                    "&pageSize=4&ipAddress=" + publicIp;
                await $.ajax({
                    url: url,
                    type: "GET",
                    headers: { token: "12345678", userId: guestUserIdLive },
                    success: function (response) {
                        if (response.data) {
                            if (response.data.postDetails.length != 0) {
                                if (response.data.pagination.isLastPage == "true") {
                                    isLastPagePost = "true"; // Fix: Change '==' to '='
                                }
                                lastPostId = response.data.pagination.lastPostedId;
                                lastPostDate = response.data.pagination.lastPostedDate;
                                var datas = response.data.postDetails;
                                datas.forEach((data) => {
                                    const postCheckId = data.post.id;
                                    let isIdInArray = false;
                                    for (let i = 0; i < liveList.length; i++) {
                                        if (liveList[i].post.id === postCheckId) {
                                            isIdInArray = true;
                                            break;
                                        }
                                    }
                                    if (!isIdInArray && data.channel.id != currentChannelId) {
                                        liveList.push(data);
                                        populateRecommendedLivePosts(data);
                                    }

                                });
                            } else {
                                console.log('0 live streams loaded');
                            }
                        }
                        else {
                            console.log('error');
                        }
                    },
                    error: function (response) {
                        console.log(response);
                    },
                });
            }
            else {
                console.log('all live streams loaded');
            }
        }

        function populateRecommendedLivePosts(item) {
            const liveContainer = document.querySelector(".desktop-view .recommendedLiveList");
            const channel = item.channel;
            const post = item.post;

            var liveUrl = baseUrl + "/live/" + "@" + channel.link;
            var channelUrl = baseUrl + "/" + "@" + channel.link;
            const profileImageUrl = channel.profileImage !== "null" ? channel.profileImage : "/assets/img/MOOD_DEFAULT.png";

            const liveElement = document.createElement('div');
            liveElement.className = 'recommendedLive';

            let htmlContent = '';
            htmlContent += '<a href="' + liveUrl + '">';
            htmlContent += '<div class="profile-box">';
            htmlContent += '<div class="overlay-bg"></div>';
            htmlContent += '<div class="overlay"></div>';
            htmlContent += '<img class="profile-pic" src="' + profileImageUrl + '" alt="profile-pic">';
            htmlContent += '<div class="liveNowBtnDiv">';
            htmlContent += '<img src="../assets/img/live_icon.svg" class="liveNowBtn" alt="live Now">';
            htmlContent += '</div>';
            htmlContent += '</div>';
            htmlContent += '</a>';
            htmlContent += '<div class="channel-header">';
            htmlContent += '<div class="channel-info">';
            htmlContent += '<div class="channel-pic">';
            htmlContent += '<a class="channel-url" href="' + channelUrl + '"> <img class="profile-image" src="' + profileImageUrl + '" alt="No Image Available" onerror="this.src=\'/assets/img/MOOD_DEFAULT.png\';"> </a>';
            if (channel.frame != null) {
                htmlContent += '<img class="recommended-profile-frame" src="' + channel.frame + '">';
            }
            htmlContent += '</div>';

            htmlContent += '<div class="channel-name-div">';
            htmlContent += '<a class="channel-link-l" href="' + channelUrl + '">';
            htmlContent += '<h5 class="channel-link">';
            htmlContent += channel.link;
            if (channel.verified == "true") {
                htmlContent += '<img class="verified-badge" src="/assets/img/verified.png">';
            }
            htmlContent += '</h5>';
            htmlContent += '</a>';
            htmlContent += '<a class="channel-name-l" href="' + channelUrl + '">';
            htmlContent += '<p class="channel-name">' + channel.name + '</p>';
            htmlContent += '</a>';
            htmlContent += '<input type="hidden" class="channel-link-input"/>';
            htmlContent += '</div>';
            htmlContent += '</div>';
            htmlContent += '</div>';

            liveElement.innerHTML = htmlContent;
            liveContainer.appendChild(liveElement);

        }

        function hideLoader() {
            $(".loader").css("display", "none");
        }

        function redirectToChannel() {
            var channelLink = $(".liveContainer").find(".channel-link-input").val();
            var channelUrl = baseUrl + "/" + "@" + channelLink;
            window.location.href = channelUrl;
        }

        function closeChatRoom() {
            handleCloseChatStyles();
            chatOpen = false;
        }

        function openChatRoom() {
            handleOpenChatStyles();
            chatOpen = true;
        }

        function handleCloseChatStyles() {
            $(".desktop-view .channel-info-main-div .chat-open-button").css("display", "flex");
            $(".desktop-view .comment-section").css("display", "none");
            $(".desktop-view .main-content").css("margin-right", "0px");
        }

        function handleOpenChatStyles() {
            $(".desktop-view .channel-info-main-div .chat-open-button").css("display", "none");
            $(".desktop-view .comment-section").css("display", "flex");
            $(".desktop-view .main-content").css("margin-right", "341px");
        }

        if ($(window).width() < 1200) {
            handleCloseChatStyles();
            $(".desktop-view .channel-info-main-div .chat-open-button").css("display", "none");
        }
        else {
            if (chatOpen) {
                openChatRoom();
            }
            else {
                closeChatRoom();
            }
        }

        // Check window width on window resize
        $(window).resize(function () {
            if ($(this).width() < 1200) {
                handleCloseChatStyles();
                $(".desktop-view .channel-info-main-div .chat-open-button").css("display", "none");
            }
            else {
                if (chatOpen) {
                    openChatRoom();
                }
                else {
                    closeChatRoom();
                }
            }
        });

        window.addEventListener("beforeunload", function (e) {
            //e.preventDefault();
            // e.returnValue = ""; // This is necessary for Chrome
            rtcClient.leave();
        });

        window.addEventListener("unload", function (e) {
            rtcClient.leave();
        });
        window.addEventListener("pagehide", function () {
            rtcClient.leave();
        });

        document.addEventListener("visibilitychange", function () {
            if (document.visibilityState === "hidden") {
                rtcClient.leave();
            } else if (document.visibilityState === "visible") {
                rtcClient.join();
            }
        });

        window.addEventListener('scroll', function () {
            if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                // When the user scrolls to the bottom of the page, load more posts
                // $(".loader-more").css("display", "flex");
                getLiveStreamsDesktop().then(function () {
                    // $(".loader-more").css("display", "none");
                    console.log('');
                });
            }
        });
    }

    function readBaseURL() {
        const currentURL = window.location.href;
        const parsedCurrentURL = new URL(currentURL);
        const baseUrl = parsedCurrentURL.origin;
        return baseUrl;
    }
});

var isMobileDevice = isMobileDeviceL();
function isMobileDeviceL() {
    return /Android|iPhone|iPad|iPod|webOS|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
    );
}

function createNewGuestDiv(channelId) {
    const guestListContainer = document.querySelector(".guestLiveDesktopContainer .guestListContainer");
    const div = document.createElement("div");
    div.id = "guest" + channelId;
    div.className = "guestDiv";
    guestListContainer.appendChild(div);
}

function createNewGuestDivMobile(channelId) {
    const guestListContainer = document.querySelector(".guestLiveMobileContainer .guestListContainerMobile");
    const div = document.createElement("div");
    div.id = "guest" + channelId;
    div.className = "guestDiv";
    guestListContainer.appendChild(div);
}

function handleLiveEnd(remoteStream, liveStreamType, channelId, channelLink) {
    if (isMobileDevice) {
        if (liveStreamType == "normal") {
            if (remoteStream.userId_ === channelId) {
                console.log('live removed');
            }
        }
        else if (liveStreamType == "guest") {
            if (remoteStream.userId_ === channelId) {
                console.log('live removed');
            } else {
                var divId = "guest" + remoteStream.userId_
                $(document).ready(function () {
                    $("#" + divId).remove();
                });;
            }
        }
    }
    else {
        if (liveStreamType == "normal") {
            if (remoteStream.userId_ === channelId) {
                console.log('live removed');
            }
        }
        else if (liveStreamType == "guest") {
            if (remoteStream.userId_ === channelId) {
                console.log('live removed');
            } else {
                var divId = "guest" + remoteStream.userId_
                $(document).ready(function () {
                    $("#" + divId).remove();
                });;
            }
        }
        else if (liveStreamType == "co-host" || liveStreamType == "pk") {
            if (remoteStream.userId_ === channelId) {
                console.log('live removed');
            }
            else {
                $(document).ready(function () {
                    $("#liveStreamhostContainer").empty();
                    $("#liveStreamCohostContainer").empty();
                })

            }
        }
    }

}

function handlePeerLeave(liveStreamType, channelId) {
    if (isMobileDevice) {
        $(".liveContainerMobile .message-box").text("Live Ended");
    }
    else {
        liveList = liveList.filter(function (item) {
            return item.channel.id !== channelId;
        });
        $(".liveContainer .message-box").text("Live Ended");
    }

}


function recallLive(channelLink) {
    console.log('stream added c');
    if (isMobileDevice) {
        window.loadSingleLiveAjaxCallMobile(channelLink);
    }
    else {
        window.loadSingleLiveAjaxCall(channelLink);
    }
}

function checkStreamType(channelLink) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            $.ajax({
                url: `${API_CONTEXT}/micro-service/live/stream-info?link=${channelLink}&activeChannelId=guest-c1772e2a-88e6-45cb-ae5d-e79ae1e01471` +
                    "&ipAddress=" + publicIp,
                type: "GET",
                headers: { token: "12345678", userId: guestUserIdLive },
                success: function (response) {
                    if (
                        response.data &&
                        response.data.post &&
                        typeof response.data.post.postType !== "undefined"
                    ) {
                        resolve(response.data.post.postType);
                    } else {
                        resolve("undefined");
                    }
                },
                error: function (response) {
                    reject("Error while fetching postType.");
                },
            });
        }, 1000);
    });
}
