/* global $ TRTC getCameraId getMicrophoneId resetView isHidden shareUserId addMemberView removeView addVideoView setAnimationFrame clearAnimationFrame*/
class RtcClient {
	constructor(options) {
		this.sdkAppId_ = 1400544070;
		this.userId_ = options.userId;
		this.userSig_ = options.userSig;
		this.roomId_ = options.roomId;
		this.privateMapKey_ = options.privateMapKey;

		this.isJoined_ = false;
		this.isPublished_ = false;
		this.isAudioMuted = false;
		this.isVideoMuted = false;
		this.localStream_ = null;
		this.remoteStreams_ = [];
		this.members_ = new Map();
		this.getAudioLevelTimer_ = -1;

		//added by himaz to increase the usercount if a web user joins the room on 31-05-2023
		this.streamId_ = options.streamId;
		this.channelLink_ = options.channelLink;
		this.channelId_ = options.channelId;
		this.cLink_ = options.cLink;
		this.recallLiveCalled = false;

		this.liveStreamType_ = options.liveStreamType;

		this.guestList = options.guestList;

		// create a client for RtcClient
		this.client_ = TRTC.createClient({
			mode: "live",
			sdkAppId: this.sdkAppId_,
			userId: this.userId_,
			userSig: this.userSig_,
			useStringRoomId: true,
		});
		this.handleEvents();
	}

	isMobileDevice() {
		return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
			navigator.userAgent
		);
	}

	async join() {


		if (this.isJoined_) {
			console.warn("duplicate RtcClient.join() observed");
			return;
		}
		try {

			var guestUserIdLive = "user" + Math.floor(Math.random() * 1000) + Date.now();
			var publicIp;

			$.getJSON('http://www.geoplugin.net/json.gp', function (data) {

				publicIp = data['geoplugin_request'];
				console.log(publicIp)
			});
			// join the room
			await this.client_.join({
				roomId: this.roomId_,
				role: "audience",
			});

			https: $.ajax({
				url:
					`${API_CONTEXT}` +
					"/micro-service/live/join?channelLink=" +
					this.channelLink_ +
					"&channelId=" +
					this.channelId_ +
					"&streamPostId=" +
					this.streamId_ +
					"&ipAddress=" + publicIp,
				type: "POST",
				headers: { token: "12345678", userId: guestUserIdLive },

				success: function (response) {
					console.log(response);
				},
				error: function (response) {
					console.log(response);
				},
			});
			this.isJoined_ = true;
		} catch (error) {
			console.error("join room failed! " + error);
		}
		this.handleEvents();
	}

	async leave() {
		var guestUserIdLive = "user" + Math.floor(Math.random() * 1000) + Date.now();
		var publicIp;

		$.getJSON('http://www.geoplugin.net/json.gp', function (data) {

			publicIp = data['geoplugin_request'];
			console.log(publicIp)
		});
		if (!this.isJoined_) {
			console.warn("leave() - please join() firstly");
			return;
		}

		await this.client_.leave();
		https: $.ajax({
			url:
				`${API_CONTEXT}` +
				"/micro-service/live/leave?channelLink=" +
				this.channelLink_ +
				"&channelId=" +
				this.channelId_ +
				"&streamPostId=" +
				this.streamId_ +
				"&ipAddress=" + publicIp,
			type: "POST",
			headers: { token: "12345678", userId: guestUserIdLive },

			success: function (response) {
				console.log(response);
			},
			error: function (response) {
				console.log(response);
			},
		});
		this.isJoined_ = false;
		// 停止获取音量
		this.stopGetAudioLevel();
		resetView();
	}

	resumeStreams() {
		for (let stream of this.remoteStreams_) {
			stream.resume();
		}
	}

	handleEvents() {
		var call = 1;
		this.client_.on("error", (err) => {
			console.error(err);
			alert(err);
			location.reload();
		});
		this.client_.on("client-banned", (err) => {
			console.error("client has been banned for " + err);
			if (!isHidden()) {
				alert("您已被踢出房间");
				location.reload();
			} else {
				document.addEventListener(
					"visibilitychange",
					() => {
						if (!isHidden()) {
							alert("您已被踢出房间");
							location.reload();
						}
					},
					false
				);
			}
		});

		// fired when a remote peer is leaving the room
		this.client_.on("peer-leave", (evt) => {
			const remoteStream = evt.stream;
			const userId = evt.userId;
			removeView(userId);
			console.log("peer-leave " + userId);
			if (this.liveStreamType_ == "normal") {
				handlePeerLeave(this.liveStreamType_, this.channelId_);
			}
			else if (this.liveStreamType_ == "guest" && userId == this.channelId_) {
				handlePeerLeave(this.liveStreamType_, this.channelId_);
			}
			else if (this.liveStreamType_ == "co-host" && userId == this.channelId_) {
				handlePeerLeave(this.liveStreamType_, this.channelId_);
			}
			else if (this.liveStreamType_ == "pk" && userId == this.channelId_) {
				handlePeerLeave(this.liveStreamType_, this.channelId_);
			}
		});
		// fired when a remote stream is added
		this.client_.on("stream-added", async (evt) => {
			const remoteStream = evt.stream;
			const id = remoteStream.getId();
			const userId = remoteStream.getUserId();
			this.members_.set(userId, remoteStream);
			console.log(
				`remote stream added: [${userId}] ID: ${id} type: ${remoteStream.getType()}`
			);
			var newPostType = await checkStreamType(this.cLink_);
			if (this.liveStreamType_ !== newPostType && !this.recallLiveCalled) {
				recallLive(this.cLink_);
				this.recallLiveCalled = true; // Set the flag to true to prevent further calls
			}
			if (remoteStream.getUserId() === shareUserId) {
				// don't need screen shared by us
				this.client_.unsubscribe(remoteStream);
			} else {
				console.log("subscribe to this remote stream");
				this.client_.subscribe(remoteStream);
			}

		});
		// fired when a remote stream has been subscribed
		this.client_.on("stream-subscribed", (evt) => {
			const uid = evt.userId;
			const remoteStream = evt.stream;
			var id;
			var isMobile = this.isMobileDevice();
			if (isMobile) {
				if (this.liveStreamType_ == "normal") {
					id = "liveStreamVideoContainerMobile";
				}
				else if (this.liveStreamType_ == "guest") {
					let isNewGuest = true;
					if (remoteStream.userId_ === this.channelId_) {
						id = "liveStreamGuestContainerMobile";
					}
					else {
						if ($("#guest" + remoteStream.userId_).length > 0) {
							var isInGuestList = false;
							for (let i = 0; i < this.guestList.length; i++) {
								if (remoteStream.userId_ === this.guestList[i].channelId) {
									isInGuestList = true;
									id = "guest" + this.guestList[i].channelId;
								}
							}
							if (isInGuestList == false) {
								id = "guest" + remoteStream.userId_;
							}
						}
						else {
							createNewGuestDivMobile(remoteStream.userId_);
							id = "guest" + remoteStream.userId_;

						}
					}



					if (isNewGuest == true) {
					}
				}
				else if (
					this.liveStreamType_ == "co-host" ||
					this.liveStreamType_ == "pk"
				) {
					if (remoteStream.userId_ === this.channelId_) {
						id = "liveStreamhostContainerMobile";
					}
					else {
						id = "liveStreamCohostContainerMobile";
					}
				}

			} else {
				if (this.liveStreamType_ == "normal") {
					id = "liveStreamVideoContainer";
				} else if (this.liveStreamType_ == "guest") {
					let isNewGuest = true;
					if (remoteStream.userId_ === this.channelId_) {
						id = "liveStreamGuestContainer";
					}
					else {
						if ($("#guest" + remoteStream.userId_).length > 0) {
							var isInGuestList = false;
							for (let i = 0; i < this.guestList.length; i++) {
								if (remoteStream.userId_ === this.guestList[i].channelId) {
									isInGuestList = true;
									id = "guest" + this.guestList[i].channelId;
								}
							}
							if (isInGuestList == false) {
								id = "guest" + remoteStream.userId_;
							}
						}
						else {
							createNewGuestDiv(remoteStream.userId_);
							id = "guest" + remoteStream.userId_;

						}
					}



					if (isNewGuest == true) {
					}
				} else if (
					this.liveStreamType_ == "co-host" ||
					this.liveStreamType_ == "pk"
				) {
					if (remoteStream.userId_ === this.channelId_) {
						id = "liveStreamhostContainer";
					}
					else {
						id = "liveStreamCohostContainer";
					}
				}
			}

			this.remoteStreams_.push(remoteStream);
			remoteStream.on("player-state-changed", (event) => {
				console.log(`${event.type} player is ${event.state}`);
			});
			addVideoView(id);
			if (remoteStream.userId_ && remoteStream.userId_.indexOf("share_") > -1) {
				remoteStream.play(id, { objectFit: "contain" }).then(() => {
					// Firefox，当video的controls设置为true的时候，video-box无法监听到click事件
					// if (getBrowser().browser === 'Firefox') {
					//   return;
					// }
					remoteStream.videoPlayer_.element_.controls = true;
				});
			} else {
				remoteStream.play(id);
			}
			//添加“摄像头未打开”遮罩
			let mask = $("#mask_main").clone();
			mask.attr("id", "mask_" + id);
			mask.appendTo($("#player_" + id));
			mask.hide();
			if (!remoteStream.hasVideo()) {
				mask.show();
				$("#" + remoteStream.getUserId())
					.find(".member-video-btn")
					.attr("src", "img/camera-off.png");
			}
			console.log("stream-subscribed ID: ", id);
		});
		// fired when the remote stream is removed, e.g. the remote user called Client.unpublish()
		this.client_.on("stream-removed", async (evt) => {
			const remoteStream = evt.stream;
			const id = remoteStream.getId();
			remoteStream.stop();
			this.remoteStreams_ = this.remoteStreams_.filter((stream) => {
				return stream.getId() !== id;
			});
			var newPostType = await checkStreamType(this.cLink_);
			if (this.liveStreamType_ !== newPostType && !this.recallLiveCalled) {
				recallLive(this.cLink_); // Set the flag to true to prevent further calls
				this.recallLiveCalled = true;
			}
			removeView(id);
			$("#" + remoteStream.getUserId())
				.find(".member-audio-btn")
				.attr("src", "img/mic-off.png");
			$("#" + remoteStream.getUserId())
				.find(".member-video-btn")
				.attr("src", "img/camera-off.png");
			console.log(`stream-removed ID: ${id}  type: ${remoteStream.getType()}`);
			handleLiveEnd(remoteStream, this.liveStreamType_, this.channelId_, this.cLink_);


		});

		this.client_.on("stream-updated", (evt) => {
			const remoteStream = evt.stream;
			let uid = this.getUidByStreamId(remoteStream.getId());
			if (!remoteStream.hasVideo()) {
				$("#" + uid)
					.find(".member-video-btn")
					.attr("src", "img/camera-off.png");
			}
			console.log(
				"type: " +
				remoteStream.getType() +
				" stream-updated hasAudio: " +
				remoteStream.hasAudio() +
				" hasVideo: " +
				remoteStream.hasVideo() +
				" uid: " +
				uid
			);
		});
	}

	showStreamState(stream) {
		console.log(
			"has audio: " + stream.hasAudio() + " has video: " + stream.hasVideo()
		);
	}

	getUidByStreamId(streamId) {
		for (let [uid, stream] of this.members_) {
			if (stream.getId() == streamId) {
				return uid;
			}
		}
	}

	startGetAudioLevel() {
		// 监听音量回调事件，更新每个用户的音量图标
		this.client_.on("audio-volume", ({ result }) => {
			result.forEach(({ userId, audioVolume }) => {
				if (audioVolume >= 10) {
					console.warn(
						`userId: ${userId} is speaking audioVolume: ${audioVolume}`
					);
					$(`#${userId === this.userId_ ? "member-me" : userId}`)
						.find(".volume-level")
						.css("height", `${audioVolume * 4}%`);
				} else {
					$(`#${userId === this.userId_ ? "member-me" : userId}`)
						.find(".volume-level")
						.css("height", `0%`);
				}
			});
		});
		this.client_.enableAudioVolumeEvaluation(100);
	}

	// 停止获取流音量
	stopGetAudioLevel() {
		this.client_.enableAudioVolumeEvaluation(-1);
	}

}
