let liveList = new Array();
var guestUserIdLive = "user" + Math.floor(Math.random() * 1000) + Date.now();
    var publicIp;

    $.getJSON('http://www.geoplugin.net/json.gp', function (data) {

        publicIp = data['geoplugin_request'];
        console.log(publicIp)
    });
$(function () {
    $("body").on("click", ".btn-download-moodapp, .getMoodAppDesktop", onClickDownloadMoodBtn);
    $("body").on("click", ".mobile-view .mobile-footer .upload-l, .mobile-view .mobile-footer .account-l", onClickDownloadMoodBtn);
    $("body").on("click", ".btn-close-download-moodapp-modal", onClickCancelDownloadMoodBtn);
    $downloadMoodappModalMobile = $("#downloadMoodappModalMobile");
    $downloadMoodappModalDesktop = $("#downloadMoodAppModalDesktop");
    var isMobile = isMobileDevice();
    function isMobileDevice() {
        return /Android|iPhone|iPad|iPod|webOS|BlackBerry|IEMobile|Opera Mini/i.test(
            navigator.userAgent
        );
    }

    function onClickDownloadMoodBtn() {
        if (isMobile) {
            $downloadMoodappModalMobile.modal("show");
        } else {
            $downloadMoodappModalDesktop.modal("show");
        }
    }
    function onClickCancelDownloadMoodBtn() {
        if (isMobile) {
            $downloadMoodappModalMobile.modal("hide");
        } else {
            $downloadMoodappModalDesktop.modal("hide");
        }
    }
    if (isMobile) {
        document.querySelector(".mobile-view").style.display = "block";

        var baseUrl = readBaseURL();
        $('.main-logo-link').attr('href', baseUrl);

        var userJid = "97466818269@jabberd-uat.moodgo.us";
        var webUserId = "user" + Math.floor(Math.random() * 1000) + Date.now();
        var channelIdForLiveList = "guest-c1772e2a-88e6-45cb-ae5d-e79ae1e01471";
        var lastPostId;
        var lastPostDate;
        var isLastPagePost = "false";
        var call = 1;
        $("body").on("click", ".primeLiveDiv", onClickPrimeLive);
        $('.liveContainerMobile .channel-header').on('click', redirectToChannel);
        $('.liveContainer').on('click', openLive);

        function redirectToChannel() {
            var channelLink = $(".liveContainer").find(".channel-link-input").val();
            var channelUrl = baseUrl + "/" + "@" + channelLink;
            window.location.href = channelUrl;
        }

        function openLive() {
            var channelLink = $(".liveContainer").find(".channel-link-input").val();
            liveUrl = baseUrl + "/live/" + "@" + channelLink;
            window.location.href = liveUrl;
        }

        processStreams();
        async function processStreams() {
            for (var i = 0; i <= 1; i++) {
                await getLiveStreamsMobile();
            }
        }

        async function getLiveStreamsMobile() {
            if (isLastPagePost == "false") {
                var url;
                url =
                    `${API_CONTEXT}` +
                    "/micro-service/live/stream?channelId=" +
                    channelIdForLiveList +
                    "&lastPostedId=" +
                    lastPostId +
                    "&lastPostedDate=" +
                    lastPostDate +
                    "&pageSize=4&ipAddress=" + publicIp;
                await $.ajax({
                    url: url,
                    type: "GET",
                    headers: { token: "12345678", userId: guestUserIdLive },
                    success: function (response) {
                        if (response.data) {
                            if (response.data.postDetails.length != 0) {
                                if (response.data.pagination.isLastPage == "true") {
                                    isLastPagePost = "true"; // Fix: Change '==' to '='
                                }
                                lastPostId = response.data.pagination.lastPostedId;
                                lastPostDate = response.data.pagination.lastPostedDate;
                                var datas = response.data.postDetails;
                                datas.forEach((data) => {
                                    liveList.push(data);
                                    if (call == 1) {
                                        populateMainLivePosts(data);
                                    }
                                    if (call > 1) {
                                        populateRecommendedLivePosts(data);
                                    }

                                });
                                if (call == 1) {
                                    hideLoaderMobile();
                                    loadFirstLiveMobile();
                                }
                            } else {
                                console.log('0 live streams loaded');
                            }
                        }
                        else {
                            console.log('error');
                        }
                    },
                    error: function (response) {
                        console.log(response);
                    },
                });
            }
            else {
                console.log('all live streams loaded');
            }
            call = call + 1;
        }

        function hideLoaderMobile() {
            $(".loader").css("display", "none");
            $(".contentMobile").css("display", "block");
        }

        function loadFirstLiveMobile() {
            const channelLink = liveList[0].channel.link;
            const channelName = liveList[0].channel.name;
            const channelImage = liveList[0].channel.profileImage;
            const channelFrame = liveList[0].channel.frame;
            const channelVerified = liveList[0].channel.verified;
            loadSingleLiveAjaxCallMobile(channelLink, channelName, channelImage, channelFrame, channelVerified);
        }

        function loadSingleLiveAjaxCallMobile(channelLink, channelName, channelImage, channelFrame, channelVerified) {
            https: $.ajax({
                url:
                    `${API_CONTEXT}` +
                    "/micro-service/live/stream-info?link=" +
                    channelLink +
                    "&activeChannelId=" +
                    channelIdForLiveList +
                    "&ipAddress=" + publicIp,
                type: "GET",
                headers: { token: "12345678", userId: guestUserIdLive },

                success: function (response) {
                    loadSingleLiveMobile(response, channelLink, channelName, channelImage, channelFrame, channelVerified);
                },
                error: function (response) { },
            });
        }
        window.loadSingleLiveAjaxCallMobile = loadSingleLiveAjaxCallMobile;

        function loadSingleLiveMobile(response, channelLink, channelName, channelImage, channelFrame, channelVerified) {
            $(".normalLiveMobileContainer").empty();
            $(".guestLiveMobileContainer").empty();
            $(".hostLiveMobileContainer").empty();
            $(".cohostLiveMobileContainer").empty();
            $(".liveContainerMobile .channel-link-l .channel-verified").css("display", "none");
            var channelUrl = baseUrl + "/" + "@" + channelLink;
            if (channelImage != undefined) {
                const profileImageUrl = channelImage !== "null" ? channelImage : "/assets/img/MOOD_DEFAULT.png";
                $(".liveContainerMobile .channel-header .profile-image").attr("src", profileImageUrl);
            }
            if (channelFrame != undefined) {
                if (channelFrame != "null" && channelFrame != null) {
                    $(".mobile-view .liveContainerMobile .main-live-profile-frame-mobile").attr("src", channelFrame);
                    $(".mobile-view .liveContainerMobile .main-live-profile-frame-mobile").css("display", "block");
                } else {
                    $(".mobile-view .liveContainerMobile .main-live-profile-frame-mobile").attr("src", "");
                    $(".main-live-profile-frame-mobile").css("display", "none");
                }
            }
            if (channelVerified != undefined) {
                if (channelVerified == "true") {
                    $(".liveContainerMobile .channel-link-l .channel-verified").css("display", "block");
                }
            }

            if (channelName != undefined) {
                $(".liveContainerMobile .channel-header .channel-name").text(channelName);
            }
            $(".liveContainerMobile .channel-header .channel-link").text(channelLink);
            $(".liveContainerMobile .channel-header .channel-url").attr("href", channelUrl);
            $(".liveContainerMobile .channel-header .channel-name-l").attr("href", channelUrl);
            $(".liveContainerMobile .channel-header.channel-link-l").attr("href", channelUrl);
            $(".liveContainer .channel-link-input").val(channelLink);

            if (!response.data) {
                $(".mobile-view .guestLiveMobileContainer").css("display", "none");
                $(".mobile-view .cohostMobileContainer").css("display", "none");
                $(".mobile-view .normalLiveMobileContainer").css("display", "block");
                $(".liveContainerMobile .message-box").text("Live Not Available Now");
                if (channelImage != "null") {
                    $(".mobile-view .normalLiveMobileContainer").css({
                        "background-image": "url(" + channelImage + ")",
                    });
                }
                else {
                    $(".mobile-view .normalLiveMobileContainer").css({
                        "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                    });
                }

            }
            else {
                const channel = response.data.channel;
                const post = response.data.post;
                if (channelName == undefined) {
                    $(".liveContainerMobile .channel-header .channel-name").text(channel.name);
                }
                if (channelImage == undefined) {
                    const profileImageUrl = channel.profileImage !== "null" ? channel.profileImage : "/assets/img/MOOD_DEFAULT.png";
                    $(".liveContainerMobile .channel-header .profile-image").attr("src", profileImageUrl);
                }
                if (channelFrame == undefined) {
                    if (channel.frame != "null" && channel.frame != null) {
                        $(".mobile-view .liveContainerMobile .main-live-profile-frame-mobile").attr("src", channel.frame);
                        $(".mobile-view .liveContainerMobile .main-live-profile-frame-mobile").css("display", "block");
                    } else {
                        $(".mobile-view .liveContainerMobile .main-live-profile-frame-mobile").attr("src", "");
                        $(".mobile-view .liveContainerMobile .main-live-profile-frame-mobile").css("display", "none");
                    }
                }
                if (channelVerified == undefined) {
                    if (channel.verified == "true") {
                        $(".liveContainerMobile .channel-link-l .channel-verified").css("display", "block");
                    }
                }

                if (post.postType == "normal") {
                    $(".mobile-view .guestLiveMobileContainer").css("display", "none");
                    $(".mobile-view .cohostMobileContainer").css("display", "none");
                    $(".mobile-view .normalLiveMobileContainer").css("display", "block");

                    if (channel.profileImage != "null") {
                        $(".mobile-view .normalLiveMobileContainer").css({
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".mobile-view .normalLiveMobileContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }

                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        cLink: channel.link,
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();
                }
                else if (post.postType == "guest") {
                    var guestListDiv = $("<div>").addClass("guestListContainerMobile");
                    $("#liveStreamGuestContainerMobile").append(guestListDiv);
                    const guestListContainer = document.querySelector(".guestLiveMobileContainer .guestListContainerMobile");
                    const existingGuestDivs = guestListContainer.querySelectorAll(".guestDiv");
                    existingGuestDivs.forEach((div) => {
                        div.remove();
                    });
                    $(".mobile-view .normalLiveMobileContainer").css("display", "none");
                    $(".mobile-view .cohostMobileContainer").css("display", "none");
                    $(".mobile-view .guestLiveMobileContainer").css("display", "block");

                    if (channel.profileImage != "null") {
                        $(".mobile-view .guestLiveMobileContainer").css({
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".mobile-view .guestLiveMobileContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    createGuestListDivsMobile(post.guest);

                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        guestList: post.guest,
                        cLink: channel.link,
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();
                }
                else if (
                    post.postType == "co-host" ||
                    post.postType == "pk"
                ) {
                    console.log(post.cohost.imageUrl);
                    $(".mobile-view .normalLiveMobileContainer").css("display", "none");
                    $(".mobile-view .guestLiveMobileContainer").css("display", "none");
                    $(".mobile-view .cohostMobileContainer").css("display", "flex");

                    if (channel.profileImage != "null") {
                        $(".mobile-view .hostLiveMobileContainer").css({
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".mobile-view .hostLiveMobileContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    if (post.cohost.imageUrl != "null") {
                        $(".mobile-view .cohostLiveMobileContainer").css({
                            "background-image": "url(" + post.cohost.imageUrl + ")",
                        });
                    }
                    else {
                        $(".mobile-view .cohostLiveMobileContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        cLink: channel.link,
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();

                }
            }

        }

        function populateMainLivePosts(item) {
            const liveContainer = document.querySelector(".majorLiveContainer");
            const channel = item.channel;
            const post = item.post;
            var channelUrl = baseUrl + "/" + "@" + channel.link;
            const profileImageUrl = channel.profileImage !== "null" ? channel.profileImage : "/assets/img/MOOD_DEFAULT.png";

            const liveElement = document.createElement('div');
            liveElement.className = 'primeLiveDiv';
            liveElement.style.backgroundImage = "url(" + channel.profileImage + ")";

            let htmlContent = '';
            htmlContent += '<div class="overlay-bg" style=\"background-image: url("' + profileImageUrl + '");\"></div>';
            htmlContent += '<div class="overlay"></div>'
            htmlContent += '<img class="profile-pic" src="' + profileImageUrl + '" alt="profile-pic"></img>';
            htmlContent += '<div class="channel-header">';
            htmlContent += '<div class="channel-info">';
            htmlContent += '<div class="channel-pic">';
            htmlContent += '<a class="channel-url" href="' + channelUrl + '"> <img class="profile-image" src="' + profileImageUrl + '" alt="No Image Available" onerror="this.src=\'/assets/img/MOOD_DEFAULT.png\';"> </a>';
            htmlContent += '<lottie-player class="lottie-live" src="/assets/lottie/Circle_v2.json" background="transparent" speed="1" loop autoplay></lottie-player>';
            if (channel.frame != null) {
                htmlContent += '<img class="major-profile-frame" src="' + channel.frame + '">';
            }
            htmlContent += ''
            htmlContent += '</div>';
            htmlContent += '<div class="channel-name-div">';
            htmlContent += '<a class="channel-link-l" href="' + channelUrl + '">';
            htmlContent += '<h5 class="channel-link">' + channel.link + '</h5>';
            if (channel.verified == "true") {
                htmlContent += '<h1><img class="verified-badge" src="assets/img/verified.png"></h1>';
            }
            htmlContent += '</a>';
            htmlContent += '<a class="channel-name-l" href="' + channelUrl + '"><p class="channel-name">' + channel.name + '</p></a>';
            htmlContent += '<input type="hidden" value="' + channel.link + '" class="channel-link-input"/>';
            htmlContent += '<input type="hidden" value="' + channel.frame + '" class="frame-input"/>';
            htmlContent += '<input type="hidden" value="' + channel.verified + '" class="verified-input"/>';
            htmlContent += '</div>';
            htmlContent += '</div>';
            htmlContent += '</div>';

            liveElement.innerHTML = htmlContent;
            liveContainer.appendChild(liveElement);
        }

        function populateRecommendedLivePosts(item) {
            const liveContainer = document.querySelector(".recommendedLiveList");
            const channel = item.channel;
            const post = item.post;

            var liveUrl = baseUrl + "/live/" + "@" + channel.link;
            var channelUrl = baseUrl + "/" + "@" + channel.link;
            const profileImageUrl = channel.profileImage !== "null" ? channel.profileImage : "/assets/img/MOOD_DEFAULT.png";

            const liveElement = document.createElement('div');
            liveElement.className = 'recommendedLive';

            let htmlContent = '';
            htmlContent += '<a href="' + liveUrl + '">';
            htmlContent += '<div class="profile-box">';
            htmlContent += '<div class="overlay-bg"></div>';
            htmlContent += '<div class="overlay"></div>';
            htmlContent += '<img class="profile-pic" src="' + profileImageUrl + '" alt="profile-pic">';
            htmlContent += '</div>';
            htmlContent += '</a>';
            htmlContent += '<div class="channel-header">';
            htmlContent += '<div class="channel-info">';
            htmlContent += '<div class="channel-pic">';
            htmlContent += '<a class="channel-url" href="' + channelUrl + '"> <img class="profile-image" src="' + profileImageUrl + '" alt="No Image Available" onerror="this.src=\'/assets/img/MOOD_DEFAULT.png\';"> </a>';
            if (channel.frame != null) {
                htmlContent += '<img class="recommended-profile-frame" src="' + channel.frame + '">';
            }
            htmlContent += '</div>';

            htmlContent += '<div class="channel-name-div">';
            htmlContent += '<a class="channel-link-l" href="' + channelUrl + '">';
            htmlContent += '<h5 class="channel-link">' + channel.link + '</h5>';
            if (channel.verified == "true") {
                htmlContent += '<h1><img class="verified-badge" src="assets/img/verified.png"></h1>';
            }
            htmlContent += '</a>';
            htmlContent += '<a class="channel-name-l" href="' + channelUrl + '">';
            htmlContent += '<p class="channel-name">' + channel.name + '</p>';
            htmlContent += '</a>';
            htmlContent += '<input type="hidden" class="channel-link-input"/>';
            htmlContent += '</div>';
            htmlContent += '</div>';
            htmlContent += '<div class="liveNowBtnDiv">';
            htmlContent += '<img src="../assets/img/live_icon.svg" class="liveNowBtn" alt="live Now">';
            htmlContent += '</div>';
            htmlContent += '</div>';

            liveElement.innerHTML = htmlContent;
            liveContainer.appendChild(liveElement);

        }

        function onClickPrimeLive() {
            if (typeof rtcClient !== 'undefined') {
                rtcClient.leave();
            }
            var primeLiveDiv = $(this);
            $(".liveContainerMobile .message-box").text("");
            var channelLink = primeLiveDiv.find(".channel-link-input").val();
            var channelName = primeLiveDiv.find(".channel-name").text();
            var channelImage = primeLiveDiv.find(".profile-image").attr("src");
            var channelFrame = primeLiveDiv.find(".frame-input").val();
            var channelVerified = primeLiveDiv.find(".verified-input").val();
            loadSingleLiveAjaxCallMobile(channelLink, channelName, channelImage, channelFrame, channelVerified);
        }

        function createGuestListDivsMobile(guestArray) {
            const guestListContainerMobile = document.querySelector(".guestLiveMobileContainer .guestListContainerMobile");
            guestListContainerMobile.style.gridTemplateColumns = `repeat(3, 1fr)`; // Set grid columns
            for (let i = guestArray.length - 1; i >= 0; i--) {
                const guest = guestArray[i];
                const div = document.createElement("div");
        
                div.id = "guest" + guest.channelId;
                div.className = "guestDiv";
                // Set inline styles to display from right to left
                div.style.gridColumn = `${3 - (i % 3)}`; // Adjust the starting column index
                guestListContainerMobile.appendChild(div);
            }
        }

        var isFetching = false;
        function isAtBottom() {
            var scrollPosition = $(window).scrollTop();
            var windowHeight = $(window).height();
            var documentHeight = $(document).height();
            return scrollPosition + windowHeight >= documentHeight - 100;
        }

        $(document).on("touchend", function () {
            $(".loader-more-mobile").css("display", "flex");
            if (isAtBottom() && !isFetching) {
                isFetching = true;
                getLiveStreamsMobile().then(function () {
                    $(".loader-more-mobile").css("display", "none");
                    isFetching = false;
                });
            }
        });

        window.addEventListener("beforeunload", function (e) {
            // e.preventDefault();
            // e.returnValue = ""; // This is necessary for Chrome
            rtcClient.leave();
        });

        window.addEventListener("unload", function (e) {
            rtcClient.leave();
        });
        window.addEventListener("pagehide", function () {
            rtcClient.leave();
        });

        document.addEventListener("visibilitychange", function () {
            if (document.visibilityState === "hidden") {
                rtcClient.leave();
            } else if (document.visibilityState === "visible") {
                rtcClient.join();
            }
        });
    }

    else {
        document.querySelector(".desktop-view").style.display = "block";

        $('.liveContainer').on('click', openLive);
        $(".liveContainer").on("click", ".next-button", nextLive);
        $(".liveContainer").on("click", ".previous-button", previousLive);
        $(".liveContainer").on("click", ".channel-header", redirectToChannel);

        var baseUrl = readBaseURL();
        var defaultIndex;
        $('.main-logo-link').attr('href', baseUrl);

        var userJid = "97466818269@jabberd-uat.moodgo.us";
        var webUserId = "user" + Math.floor(Math.random() * 1000) + Date.now();
        var channelIdForLiveList = "guest-c1772e2a-88e6-45cb-ae5d-e79ae1e01471";
        var lastPostId;
        var lastPostDate;
        var isLastPagePost = "false";
        var call = 1;
        var currentLiveIndex = 0;

        getLiveStreams();
        async function getLiveStreams() {
            if (isLastPagePost == "false") {
                var url;
                url =
                    `${API_CONTEXT}` +
                    "/micro-service/live/stream?channelId=" +
                    channelIdForLiveList +
                    "&lastPostedId=" +
                    lastPostId +
                    "&lastPostedDate=" +
                    lastPostDate +
                    "&pageSize=4&ipAddress=" + publicIp;
                await $.ajax({
                    url: url,
                    type: "GET",
                    headers: { token: "12345678", userId: guestUserIdLive },
                    success: function (response) {
                        if (response.data) {
                            if (response.data.postDetails.length != 0) {
                                if (response.data.pagination.isLastPage == "true") {
                                    isLastPagePost = "true"; // Fix: Change '==' to '='
                                }
                                lastPostId = response.data.pagination.lastPostedId;
                                lastPostDate = response.data.pagination.lastPostedDate;
                                var datas = response.data.postDetails;
                                datas.forEach((data) => {
                                    const channelCheckID = data.channel.id;
                                    let isIDInArray = false;
                                    for (let i = 0; i < liveList.length; i++) {
                                        if (liveList[i].channel.id == channelCheckID) {
                                            isIDInArray = true;
                                            break;
                                        }
                                    }
                                    if (isIDInArray == false) {
                                        liveList.push(data);
                                        console.log('hideLoader');
                                    }
                                });
                                if (call == 1) {
                                    loadFirstLive();
                                }
                            } else {
                                console.log('0 live streams loaded');
                            }
                        }
                        else {
                            console.log('error');
                        }
                    },
                    error: function (response) {
                        console.log(response);
                    },
                });
            }
            else {
                console.log('all live streams loaded');
            }
            call = call + 1;
        }
        function loadFirstLive() {
            const channelLink = liveList[currentLiveIndex].channel.link;
            loadSingleLiveAjaxCall(channelLink);
        }

        function loadSingleLiveAjaxCall(channelLink) {

            https: $.ajax({
                url:
                    `${API_CONTEXT}` +
                    "/micro-service/live/stream-info?link=" +
                    channelLink +
                    "&activeChannelId=" +
                    channelIdForLiveList +
                    "&ipAddress=" + publicIp,
                type: "GET",
                headers: { token: "12345678", userId: guestUserIdLive },

                success: function (response) {
                    loadSingleLive(response, channelLink);
                },
                error: function (response) { },
            });
        }
        window.loadSingleLiveAjaxCall = loadSingleLiveAjaxCall;

        function loadSingleLive(response, channelLink) {
            var data;
            if (response.status == 200) {
                data = response.data;
            }
            $(".normalLiveDesktopContainer").empty();
            $(".guestLiveDesktopContainer").empty();
            $(".hostLiveDesktopContainer").empty();
            $(".cohostLiveDesktopContainer").empty();
            if (!response.data) {
                $(".liveContainer .message-box").text("Live Not Available Now");
                $(".desktop-view .normalLiveDesktopContainer").css({
                    "background-repeat": "no-repeat",
                    "background-size": "cover",
                    "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                });
                $(".desktop-view .guestLiveDesktopContainer").css({
                    "background-repeat": "no-repeat",
                    "background-size": "cover",
                    "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                });
                $('.liveContainer .profile-image').attr('src', '/assets/img/MOOD_DEFAULT.png');
                $(".liveContainer .main-live-profile-frame").attr('src', '');
                $(".liveContainer .main-live-profile-frame").css("display", "none");
                $(".liveContainer .channel-name").text("");
                $(".liveContainer .channel-link").text("");
                liveList = liveList.filter(function (item) {
                    return item.channel.link !== channelLink;
                });
            }
            else {
                hideLoader();
                const channel = response.data.channel;
                const post = response.data.post;
                $(".liveContainer .message-box").text("");
                $(".liveContainer .channel-name").text(channel.name);
                assignChannelLinkAndBatches(channel);
                $(".liveContainer .channel-link-input").val(channel.link);
                defaultIndex = liveList.findIndex(function (obj) {
                    return obj.channel.link === channel.link;
                });

                if (channel.profileImage != "null") {
                    $('.liveContainer .profile-image').attr('src', channel.profileImage);
                }
                else {
                    $('.liveContainer .profile-image').attr('src', '/assets/img/MOOD_DEFAULT.png');
                }
                if (channel.frame != null) {
                    $(".liveContainer .main-live-profile-frame").attr('src', channel.frame);
                    $(".liveContainer .main-live-profile-frame").css("display", "block");
                }
                else {
                    $(".liveContainer .main-live-profile-frame").attr('src', '');
                    $(".liveContainer .main-live-profile-frame").css("display", "none");
                }

                $(".liveContainer").css("display", "flex");
                //Normal Live Begins
                if (post.postType == "normal") {
                    $(".desktop-view .guestLiveDesktopContainer").css("display", "none");
                    $(".desktop-view .cohostDesktopContainer").css("display", "none");
                    $(".desktop-view .normalLiveDesktopContainer").css("display", "block");
                    if (channel.profileImage != "null") {
                        $(".desktop-view .normalLiveDesktopContainer").css({
                            "background-repeat": "no-repeat",
                            "background-size": "cover",
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".desktop-view .normalLiveDesktopContainer").css({
                            "background-repeat": "no-repeat",
                            "background-size": "cover",
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }

                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        cLink: channel.link,
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();

                }
                //Normal Live Ends

                //Guest Live Begins
                else if (post.postType == "guest") {
                    var guestListDiv = $("<div>").addClass("guestListContainer");
                    $("#liveStreamGuestContainer").append(guestListDiv);
                    const guestListContainer = document.querySelector(".guestLiveDesktopContainer .guestListContainer");
                    const existingGuestDivs = guestListContainer.querySelectorAll(".guestDiv");
                    existingGuestDivs.forEach((div) => {
                        div.remove();
                    });
                    $(".desktop-view .normalLiveDesktopContainer").css("display", "none");
                    $(".desktop-view .cohostDesktopContainer").css("display", "none");
                    $(".desktop-view .guestLiveDesktopContainer").css("display", "block");
                    if (channel.profileImage != "null") {
                        $(".desktop-view .guestLiveDesktopContainer").css({
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".desktop-view .guestLiveDesktopContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    createGuestListDivs(post.guest);

                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        cLink: channel.link,
                        guestList: post.guest
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();
                }
                //Guest Live Ends

                //Cohost Live Starts
                else if (
                    post.postType == "co-host" ||
                    post.postType == "pk"
                ) {

                    $(".desktop-view .normalLiveDesktopContainer").css("display", "none");
                    $(".desktop-view .guestLiveDesktopContainer").css("display", "none");
                    $(".desktop-view .cohostDesktopContainer").css("display", "flex");

                    if (channel.profileImage != "null") {
                        $(".desktop-view .hostLiveDesktopContainer").css({
                            "background-image": "url(" + channel.profileImage + ")",
                        });
                    }
                    else {
                        $(".desktop-view .hostLiveDesktopContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    if (post.cohost.imageUrl != "null") {
                        $(".desktop-view .cohostLiveDesktopContainer").css({
                            "background-image": "url(" + post.cohost.imageUrl + ")",
                        });
                    }
                    else {
                        $(".desktop-view .cohostLiveDesktopContainer").css({
                            "background-image": "url(/assets/img/MOOD_DEFAULT.png)",
                        });
                    }
                    var userIdTrtc = "User_" + Math.floor(Math.random() * 1000000);
                    var dataUserSignature = genTestUserSig(userIdTrtc);

                    var option = {
                        sdkAppId: dataUserSignature.sdkAppId,
                        userId: userIdTrtc,
                        userSig: dataUserSignature.userSig,
                        roomId: post.videoUrl,
                        streamId: post.id,
                        channelLink: webUserId,
                        channelId: channel.id,
                        liveStreamType: post.postType,
                        cLink: channel.link,
                    };
                    rtcClient = new RtcClient(option);
                    rtcClient.join();
                }
                //Co host live ends
            }

        }

        function createGuestListDivs(guestArray) {
            const guestListContainer = document.querySelector(".guestLiveDesktopContainer .guestListContainer");
            guestListContainer.style.gridTemplateColumns = `repeat(3, 1fr)`; // Set grid columns
            for (let i = guestArray.length - 1; i >= 0; i--) {
                const guest = guestArray[i];
                const div = document.createElement("div");
        
                div.id = "guest" + guest.channelId;
                div.className = "guestDiv";
                // Set inline styles to display from right to left
                div.style.gridColumn = `${3 - (i % 3)}`; // Adjust the starting column index
                guestListContainer.appendChild(div);
            }
        }

        function hideLoader() {
            $(".loader").css("display", "none");
        }

        function nextLive(event) {
            event.stopPropagation();

            if (liveList.length > 0) {
                if (currentLiveIndex < liveList.length - 1) {
                    currentLiveIndex++;
                    const channelLink = liveList[currentLiveIndex].channel.link;
                    rtcClient.leave().then(() => {
                        loadSingleLiveAjaxCall(channelLink);
                    });
                }
                else {
                    getLiveStreams();
                }
            }

        }

        function previousLive(event) {
            event.stopPropagation();

            if (liveList.length > 0) {
                if (currentLiveIndex > 0) {
                    currentLiveIndex--;
                    const channelLink = liveList[currentLiveIndex].channel.link;
                    rtcClient.leave().then(() => {
                        loadSingleLiveAjaxCall(channelLink);
                    });
                }
            }
        }

        function openLive() {
            var channelLink = $(".liveContainer").find(".channel-link-input").val();
            liveUrl = baseUrl + "/live/" + "@" + channelLink;
            window.location.href = liveUrl;
        }

        function redirectToChannel() {
            var channelLink = $(".liveContainer").find(".channel-link-input").val();
            var channelUrl = baseUrl + "/" + "@" + channelLink;
            window.location.href = channelUrl;
        }

        function assignChannelLinkAndBatches(channel) {
            $(".liveContainer .channel-link").text(channel.link).append(channel.verified == "true" ? $('<img>').attr('src', '/assets/img/verified.png').attr('alt', 'Verified Batch').css({
                marginLeft: '5px',
                height: '20px',
                verticalAlign: 'middle'
            }) : '',
                channel.badge === "1" ? $('<img>').attr('src', '/assets/img/bronze.png').attr('alt', 'Bronze Batch').css({
                    marginLeft: '5px',
                    height: '20px',
                    verticalAlign: 'middle'
                }) : '',
                channel.badge === "2" ? $('<img>').attr('src', '/assets/img/silver.png').attr('alt', 'Silver Batch').css({
                    marginLeft: '5px',
                    height: '20px',
                    verticalAlign: 'middle'
                }) : '',
                channel.badge === "3" ? $('<img>').attr('src', '/assets/img/gold.png').attr('alt', 'Gold Batch').css({
                    marginLeft: '5px',
                    height: '20px',
                    verticalAlign: 'middle'
                }) : '',
                channel.badge === "4" ? $('<img>').attr('src', '/assets/img/silver.png').attr('alt', 'Silver Batch').css({
                    marginLeft: '5px',
                    height: '20px',
                    verticalAlign: 'middle'
                }) : '',
                channel.badge === "4" ? $('<img>').attr('src', '/assets/img/gold.png').attr('alt', 'Gold Batch').css({
                    marginLeft: '0px',
                    height: '20px',
                    verticalAlign: 'middle'
                }) : '',
                channel.badge === "5" ? $('<img>').attr('src', '/assets/img/bronze.png').attr('alt', 'Bronze Batch').css({
                    marginLeft: '5px',
                    height: '20px',
                    verticalAlign: 'middle'
                }) : '',
                channel.badge === "5" ? $('<img>').attr('src', '/assets/img/silver.png').attr('alt', 'Silver Batch').css({
                    marginLeft: '0px',
                    height: '20px',
                    verticalAlign: 'middle'
                }) : '',
                channel.badge === "5" ? $('<img>').attr('src', '/assets/img/gold.png').attr('alt', 'Gold Batch').css({
                    marginLeft: '0px',
                    height: '20px',
                    verticalAlign: 'middle'
                }) : '');
        }


        window.addEventListener("beforeunload", function (e) {
            //e.preventDefault();
            // e.returnValue = ""; // This is necessary for Chrome
            rtcClient.leave();
        });

        window.addEventListener("unload", function (e) {
            rtcClient.leave();
        });
        window.addEventListener("pagehide", function () {
            rtcClient.leave();
        });

        document.addEventListener("visibilitychange", function () {
            if (document.visibilityState === "hidden") {
                rtcClient.leave();
            } else if (document.visibilityState === "visible") {
                rtcClient.join();
            }
        });
    }

    function readBaseURL() {
        const currentURL = window.location.href;
        const parsedCurrentURL = new URL(currentURL);
        const baseUrl = parsedCurrentURL.origin;
        return baseUrl;
    }
});

var isMobileDevice = isMobileDeviceL();
function isMobileDeviceL() {
    return /Android|iPhone|iPad|iPod|webOS|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
    );
}

function createNewGuestDiv(channelId) {
    const guestListContainer = document.querySelector(".guestLiveDesktopContainer .guestListContainer");
    const div = document.createElement("div");
    div.id = "guest" + channelId;
    div.className = "guestDiv";
    guestListContainer.appendChild(div);
}

function createNewGuestDivMobile(channelId) {
    const guestListContainer = document.querySelector(".guestLiveMobileContainer .guestListContainerMobile");
    const div = document.createElement("div");
    div.id = "guest" + channelId;
    div.className = "guestDiv";
    guestListContainer.appendChild(div);
}

function handleLiveEnd(remoteStream, liveStreamType, channelId, channelLink) {
    if (isMobileDevice) {
        if (liveStreamType == "normal") {
            if (remoteStream.userId_ === channelId) {
                console.log('live removed');
            }
        }
        else if (liveStreamType == "guest") {
            if (remoteStream.userId_ === channelId) {
                console.log('live removed');
            } else {
                var divId = "guest" + remoteStream.userId_
                $(document).ready(function () {
                    $("#" + divId).remove();
                });;
            }
        }
    }
    else {
        if (liveStreamType == "normal") {
            if (remoteStream.userId_ === channelId) {
                console.log('live removed');
            }
        }
        else if (liveStreamType == "guest") {
            if (remoteStream.userId_ === channelId) {
                console.log('live removed');
            } else {
                var divId = "guest" + remoteStream.userId_
                $(document).ready(function () {
                    $("#" + divId).remove();
                });;
            }
        }
        else if (liveStreamType == "co-host" || liveStreamType == "pk") {
            if (remoteStream.userId_ === channelId) {
                console.log('live removed');
            }
            else {
                $(document).ready(function () {
                    $("#liveStreamhostContainer").empty();
                    $("#liveStreamCohostContainer").empty();
                })

            }
        }
    }

}

function handlePeerLeave(liveStreamType, channelId) {
    if (isMobileDevice) {
        $(".liveContainerMobile .message-box").text("Live Ended");
    }
    else {
        liveList = liveList.filter(function (item) {
            return item.channel.id !== channelId;
        });
        $(".liveContainer .message-box").text("Live Ended");
    }

}


function recallLive(channelLink) {
    console.log('stream added c');
    if (isMobileDevice) {
        window.loadSingleLiveAjaxCallMobile(channelLink);
    }
    else {
        window.loadSingleLiveAjaxCall(channelLink);
    }
}

function checkStreamType(channelLink) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            $.ajax({
                url: `${API_CONTEXT}/micro-service/live/stream-info?link=${channelLink}&activeChannelId=guest-c1772e2a-88e6-45cb-ae5d-e79ae1e01471` +
                    "&ipAddress=" + publicIp,
                type: "GET",
                headers: { token: "12345678", userId: guestUserIdLive },
                success: function (response) {
                    if (
                        response.data &&
                        response.data.post &&
                        typeof response.data.post.postType !== "undefined"
                    ) {
                        resolve(response.data.post.postType);
                    } else {
                        resolve("undefined");
                    }
                },
                error: function (response) {
                    reject("Error while fetching postType.");
                },
            });
        }, 1000);
    });
}
