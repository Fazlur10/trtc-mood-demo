In our project, we have implemented live streaming functionality using multiple files across different directories. Here is an overview of the key components involved:

**JSP Files**

- liveStream.jsp:
    - This file is used for viewing a single live stream.

- index.jsp:
    - This file displays a list of live streams.


**JavaScript Files**

- Live Folder:

1. index.js: Implements the live streaming functionality for the index.jsp page.
2. liveStream.js: Implements the live streaming functionality for the liveStream.jsp page.
3. rtc-client-live.js:
    - Used to create the rtcClient instance.
    - Handles joining and leaving live streams.

    
- js Folder:

1. trtc.js: Contains the TRTC SDK.

Additional Information
- The implementation involves many partial files not included here to keep the explanation concise.
- The provided files are the main ones involved in the live streaming feature.
- There may be other related JavaScript files and necessary files not mentioned here, but the key components for understanding the implementation are covered.
