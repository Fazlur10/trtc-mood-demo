<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="../partials/setSecurityHeaders.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <title>MOOD LIVE | MOOD™ go</title>
    <%@include file="../partials/meta.jsp" %> 
    <link rel="stylesheet" href="${context}/assets/css/live/live.css" />
    <meta name="apicontext" content="${apiContext}" />
</head>

<body>
    <div class="desktop-view" style="display: none;">
        <%@include file="../partials/navbarDesktop.jsp" %>

            <div class="main-content-livepage">
                <%@include file="../partials/sidebarDesktop.jsp" %>
                    <div class="content main-content">
                        <div class="liveContainer">
                            <div id="liveStreamVideoContainer" class="normalLiveDesktopContainer">

                            </div>
                            <div id="liveStreamGuestContainer" class="guestLiveDesktopContainer">
                                <div class="guestListContainer">
                                </div>
                            </div>
                            <div class="cohostDesktopContainer">
                                <div id="liveStreamhostContainer" class="hostLiveDesktopContainer">
                                </div>
                                <div id="liveStreamCohostContainer" class="cohostLiveDesktopContainer">
                                </div>
                            </div>
                            <div class="message-box">
                            </div>
                            <div class="open-div-box">
                                <div class="open-div-box-1"></div>
                                <div class="open-div-box-2">Click to watch Live</div>
                                <div class="open-div-box-3"></div>
                            </div>
                            <div class="navigation-box">
                                <div class="previous">
                                    <button class="previous-button"><img src="/assets/img/uparrow.png" width="20"
                                            alt="previous"></button>
                                </div>
                                <div class="next">
                                    <button class="next-button"><img src="/assets/img/downarrow.png" width="20"
                                            alt="next"></button>
                                </div>
                            </div>
                            <div class="channel-header">
                                <div class="channel-info">
                                    <div class="channel-pic">
                                        <a class="channel-url" href=""> <img class="profile-image"
                                                src="/assets/img/MOOD_DEFAULT.png" alt="No Image Available"
                                                onerror="this.src='/assets/img/MOOD_DEFAULT.png';">
                                            <img class="main-live-profile-frame" src="">
                                            <lottie-player class="lottie-live"
                                                src="${context}/assets/lottie/Circle_v2.json" background="transparent"
                                                speed="1" loop autoplay></lottie-player></a>
                                    </div>
                                    <div class="channel-name-div">
                                        <a class="channel-link-l" href="">
                                            <h5 class="channel-link">Channel Name</h5>
                                        </a>
                                        <a class="channel-name-l" href="">
                                            <p class="channel-name">channel_link</p>
                                        </a>
                                        <input type="hidden" class="channel-link-input" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

            <%@include file="../partials/downloadModalDesktop.jsp" %>

    </div>
    <div class="loader">
        <lottie-player src="/assets/lottie/moodLoading.json" background="transparent" speed="1"
            style="width: 200px; height: 200px;" loop autoplay></lottie-player>
    </div>


    <!--Mobile-->
    <div class="mobile-view" style="display: none;">
        <!--Mobile Navbar-->
        <div class="navbar">
            <div class="main-logo">
                <a class="main-logo-link"> <img src="/assets/img/mood-logo-blue.svg" alt="mooders" width="250px"> </a>
            </div>
            <div class="download-top">
                <button class="btn btn-sm btn-download-moodapp">Download MOOD</button>
            </div>
        </div>
        <div class="contentMobile">
            <div class="liveContainerMobile">
                <div class="liveContainer">
                    <div id="liveStreamVideoContainerMobile" class="normalLiveMobileContainer">

                    </div>
                    <div id="liveStreamGuestContainerMobile" class="guestLiveMobileContainer">
                        <div class="guestListContainerMobile">
                        </div>
                    </div>
                    <div class="cohostMobileContainer">
                        <div id="liveStreamhostContainerMobile" class="hostLiveMobileContainer">
                        </div>
                        <div id="liveStreamCohostContainerMobile" class="cohostLiveMobileContainer">
                        </div>
                    </div>
                </div>

                <div class="message-box">
                </div>

                <div class="channel-header">
                    <div class="channel-info">
                        <div class="channel-pic">
                            <a class="channel-url" href=""> <img class="profile-image"
                                    src="/assets/img/MOOD_DEFAULT.png" alt="No Image Available"
                                    onerror="this.src='/assets/img/MOOD_DEFAULT.png';"> </a>
                            <lottie-player class="lottie-live" src="${context}/assets/lottie/Circle_v2.json"
                                background="transparent" speed="1" loop autoplay></lottie-player>
                            <img class="main-live-profile-frame-mobile" src="">

                        </div>
                        <div class="channel-name-div">
                            <a class="channel-link-l" href="">
                                <h5 class="channel-link"></h5>
                                <h5 class="channel-verified"><img class="verified-badge" src="assets/img/verified.png">
                                </h5>
                            </a>
                            <a class="channel-name-l" href="">
                                <p class="channel-name"></p>
                            </a>
                            <input type="hidden" class="channel-link-input" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="majorLiveContainer">

                <!--More primeLiveDiv goes here-->
            </div>

            <div class="recommendedLiveContainer">
                <h1 class="heading">Recommended Live Videos</h1>
                <div class="recommendedLiveList">
                    <!-- <div class="recommendedLive">
                            <a href="">
                                <div class="profile-box">
                                    <div class="overlay-bg"></div>
                                    <div class="overlay"></div>
                                    <img class="profile-pic" src="https://moodapp-mediafiles-qa.s3.me-south-1.amazonaws.com/mood/media/channel/post-thumbnail/97466946146/16944231782681694423178106Download(29).jpg" alt="profile-pic">
                                </div>
                            </a>
                            <div class="channel-header">
                                <div class="channel-info">
                                    <div class="channel-pic">
                                       <a class="channel-url" href=""> <img class="profile-image" src="/assets/img/MOOD_DEFAULT.png" alt="No Image Available"> </a>
                                                    
                                    </div>
                                    <div class="channel-name-div">
                                        <a class="channel-name-l" href=""><h5 class="channel-name">Channel Name</h5></a>
                                        <a class="channel-link-l" href=""><p class="channel-link">@channel_link</p></a> 
                                        <input type="hidden" class="channel-link-input"/>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                </div>
            </div>


        </div>




        <%@include file="../partials/downloadModalMobile.jsp" %>

            <div class="mobile-footer">
                <div class="footer-nav">
                    <a class="for-you-l">
                        <img src="/assets/img/nav/home1.svg" alt="home">
                        <p> home </p>
                    </a>
                </div>
                <div class="footer-nav">
                    <a class="explore-l">
                        <img src="/assets/img/nav/friends1.svg" alt="friends">
                        <p> friends </p>
                    </a>
                </div>
                <div class="footer-nav">
                    <a class="live-l">
                        <img src="/assets/img/nav/live1.svg" alt="live">
                        <p> live </p>
                    </a>
                </div>
                <div class="footer-nav">
                    <a class="account-l">
                        <img src="/assets/img/nav/you1.svg" alt="user">
                        <p> user </p>
                    </a>
                </div>
            </div>

            <div class="loader-more-mobile">
                <lottie-player src="/assets/lottie/moodLoading.json" background="transparent" speed="1"
                    style="width: 150px; height: 150px;" loop autoplay></lottie-player>
            </div>
            <!-- <div class="footer"></div> -->
    </div>
    
    <%@include file="../partials/sharedScripts.jsp" %>
    <script src="${context}/assets/js/Live/index.js"></script>
    <script src="${context}/assets/js/config.js"></script>
    <script src="${context}/assets/js/debug/GenerateTestUserSig.js"></script>
    <script src="${context}/assets/js/common.js"></script>
    <script src="${context}/assets/js/device-testing.js"></script>
    <script src="${context}/assets/js/iconfont.js"></script>
    <script src="${context}/assets/js/lib-generate-test-usersig.min.js"></script>
    <script src="${context}/assets/js/presetting.js"></script>
    <script src="${context}/assets/js/Live/rtc-client-live.js"></script>
    <script src="${context}/assets/js/rtc-detection.js"></script>
    <script src="${context}/assets/js/trtc.js"></script>
    <script src="${context}/assets/js/device-uuid.min.js"></script>
    <script src="${context}/assets/js/nav.js"></script>
    <script src="${context}/assets/js/Login/login-primary.js"></script>
    <script src="${context}/assets/js/Search/navSearch.js"></script>

</body>

</html>
