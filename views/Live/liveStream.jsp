<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@include file="../partials/setSecurityHeaders.jsp" %>
            <!DOCTYPE html>
            <html>

            <head>
                <title>MOOD LIVE | MOOD™ go</title>
                <%@include file="../partials/meta.jsp" %>
                    <link rel="stylesheet" href="${context}/assets/css/live/liveStream.css" />
                    <meta name="apicontext" content="${apiContext}" />
            </head>

            <body>
                <input id="channel_link_for_Stream" type="hidden" value="${channelLink}" />
                <input id="channel_id_for_stream" type="hidden" value="${channel.id}" />
                <div class="desktop-view" style="display: none;">
                    <%@include file="../partials/navbarDesktop.jsp" %>

                        <div class="main-content-livepage">
                            <%@include file="../partials/sidebarDesktop.jsp" %>
                                <div class="content main-content">
                                    <div class="channel-info-main-div">
                                        <div class="channel-header">
                                            <div class="channel-info">
                                                <div class="channel-pic">
                                                    <a class="channel-url" href=""> <img class="profile-image"
                                                            src="${channel.image}" alt="No Image Available"
                                                            onerror="this.src='/assets/img/MOOD_DEFAULT.png';">
                                                        <!-- <img class="main-live-profile-frame" src=""> -->
                                                        <lottie-player class="lottie-live"
                                                            src="${context}/assets/lottie/Circle_v2.json"
                                                            background="transparent" speed="1" loop
                                                            autoplay></lottie-player></a>
                                                </div>
                                                <div class="channel-name-div">
                                                    <a class="channel-link-l" href="">
                                                        <h5 class="channel-link">${channelLink}
                                                            <c:if test="${channel.verified == 1}">
                                                                <img class="verified-badge"
                                                                    src="/assets/img/verified.png" alt="Verified Badge">
                                                            </c:if>
                                                            <c:choose>
                                                                <c:when test="${channel.badge == 1}">
                                                                    <img class="badge-image"
                                                                        src="/assets/img/bronze.png" alt="Bronze Badge">
                                                                </c:when>
                                                                <c:when test="${channel.badge == 2}">
                                                                    <img class="badge-image"
                                                                        src="/assets/img/silver.png" alt="Silver Badge">
                                                                </c:when>
                                                                <c:when test="${channel.badge == 3}">
                                                                    <img class="badge-image" src="/assets/img/gold.png"
                                                                        alt="Gold Badge">
                                                                </c:when>
                                                                <c:when test="${channel.badge == 4}">
                                                                    <img class="badge-image"
                                                                        src="/assets/img/silver.png" alt="Silver Badge">
                                                                    <img class="badge-image" src="/assets/img/gold.png"
                                                                        alt="Gold Badge">
                                                                </c:when>
                                                                <c:when test="${channel.badge == 5}">
                                                                    <img class="badge-image"
                                                                        src="/assets/img/bronze.png" alt="Bronze Badge">
                                                                    <img class="badge-image"
                                                                        src="/assets/img/silver.png" alt="Silver Badge">
                                                                    <img class="badge-image" src="/assets/img/gold.png"
                                                                        alt="Gold Badge">
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <!-- Default case when badge is not 1, 2, or 3 -->
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </h5>
                                                    </a>
                                                    <a class="channel-name-l" href="">
                                                        <p class="channel-name">${channel.name}</p>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="action-container">
                                            <i data-e2e="share-icon" tabindex="0" role="button"
                                                class="mood-IActionButton">
                                                <img width="20" src="${context}/assets/img/forward.png" />
                                            </i>
                                            <div data-e2e="live-room-follow" class="live-follow-div">
                                                <button type="button" class="follow-button">
                                                    <span>Follow</span>
                                                </button>
                                            </div>
                                            <i data-e2e="chat-open-button" tabindex="0" role="button"
                                                class="chat-open-button">
                                                <img alt="->" width="18" height="18"
                                                    src="${context}/assets/img/left-arrow-black.png" />
                                            </i>
                                        </div>
                                    </div>
                                    <div class="liveContainer">
                                        <input type="hidden" class="channel-link-input" />
                                        <div id="liveStreamVideoContainer" class="normalLiveDesktopContainer">

                                        </div>
                                        <div id="liveStreamGuestContainer" class="guestLiveDesktopContainer">
                                            <div class="guestListContainer">
                                            </div>
                                        </div>
                                        <div class="cohostDesktopContainer">
                                            <div id="liveStreamhostContainer" class="hostLiveDesktopContainer">
                                            </div>
                                            <div id="liveStreamCohostContainer" class="cohostLiveDesktopContainer">
                                            </div>
                                        </div>
                                        <div class="message-box">
                                        </div>

                                        <!-- <div class="navigation-box">
                                            <div class="previous">
                                                <button class="previous-button"><img src="/assets/img/uparrow.png"
                                                        width="20" alt="previous"></button>
                                            </div>
                                            <div class="next">
                                                <button class="next-button"><img src="/assets/img/downarrow.png"
                                                        width="20" alt="next"></button>
                                            </div>
                                        </div> -->

                                    </div>
                                    <div class="comment-section">
                                        <div class="chat-room-header">
                                            <div class="chat-close-button">
                                                <img alt="->" width="18" height="18"
                                                    src="${context}/assets/img/right-arrow-black.png" />
                                            </div>
                                            <div class="chat-room-title">LIVE chat</div>
                                        </div>
                                        <div class="chat-room-content">
                                            <div class="chat-room-container">
                                                <div class="chat-room-body">

                                                </div>
                                                <div class="comment-container">
                                                    <div class="download-to-comment">Get Mood app to comment</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="recommendedLiveContainer">
                                        <h1 class="heading">Recommended Live Videos</h1>
                                        <div class="recommendedLiveList">

                                        </div>
                                    </div>

                                </div>
                        </div>

                        <%@include file="../partials/downloadModalDesktop.jsp" %>

                </div>
                <div class="loader">
                    <lottie-player src="/assets/lottie/moodLoading.json" background="transparent" speed="1"
                        style="width: 200px; height: 200px;" loop autoplay></lottie-player>
                </div>


                <!--Mobile-->
                <div class="mobile-view" style="display: none;">
                    <!--Mobile Navbar-->
                    <div class="navbar">
                        <div class="main-logo">
                            <a class="main-logo-link"> <img src="/assets/img/mood-logo-blue.svg" alt="mooders"
                                    width="250px"> </a>
                        </div>
                        <div class="download-top">
                            <button class="btn btn-sm btn-download-moodapp">Download MOOD</button>
                        </div>
                    </div>
                    <div class="contentMobile">
                        <div class="liveContainerMobile">
                            <div class="liveContainer">
                                <div id="liveStreamVideoContainerMobile" class="normalLiveMobileContainer">

                                </div>
                                <div id="liveStreamGuestContainerMobile" class="guestLiveMobileContainer">
                                    <div class="guestListContainerMobile">
                                    </div>
                                </div>
                                <div class="cohostMobileContainer">
                                    <div id="liveStreamhostContainerMobile" class="hostLiveMobileContainer">
                                    </div>
                                    <div id="liveStreamCohostContainerMobile" class="cohostLiveMobileContainer">
                                    </div>
                                </div>
                            </div>

                            <div class="message-box">
                            </div>

                            <div class="channel-main-banner">
                                <div class="channel-header">
                                    <div class="channel-info">
                                        <div class="channel-pic">
                                            <a class="channel-url" href=""> <img class="profile-image"
                                                    src="${channel.image}" alt="No Image Available"
                                                    onerror="this.src='/assets/img/MOOD_DEFAULT.png';"> </a>
                                            <lottie-player class="lottie-live"
                                                src="${context}/assets/lottie/Circle_v2.json" background="transparent"
                                                speed="1" loop autoplay></lottie-player>
                                            <img class="main-live-profile-frame-mobile" src="">

                                        </div>
                                        <div class="channel-name-div">
                                            <a class="channel-link-l" href="">
                                                <h5 class="channel-link">${channelLink}
                                                    <c:if test="${channel.verified == 1}">
                                                        <img class="verified-badge" src="/assets/img/verified.png"
                                                            alt="Verified Badge">
                                                    </c:if>
                                                    <c:choose>
                                                        <c:when test="${channel.badge == 1}">
                                                            <img class="badge-image" src="/assets/img/bronze.png"
                                                                alt="Bronze Badge">
                                                        </c:when>
                                                        <c:when test="${channel.badge == 2}">
                                                            <img class="badge-image" src="/assets/img/silver.png"
                                                                alt="Silver Badge">
                                                        </c:when>
                                                        <c:when test="${channel.badge == 3}">
                                                            <img class="badge-image" src="/assets/img/gold.png"
                                                                alt="Gold Badge">
                                                        </c:when>
                                                        <c:when test="${channel.badge == 4}">
                                                            <img class="badge-image" src="/assets/img/silver.png"
                                                                alt="Silver Badge">
                                                            <img class="badge-image" src="/assets/img/gold.png"
                                                                alt="Gold Badge">
                                                        </c:when>
                                                        <c:when test="${channel.badge == 5}">
                                                            <img class="badge-image" src="/assets/img/bronze.png"
                                                                alt="Bronze Badge">
                                                            <img class="badge-image" src="/assets/img/silver.png"
                                                                alt="Silver Badge">
                                                            <img class="badge-image" src="/assets/img/gold.png"
                                                                alt="Gold Badge">
                                                        </c:when>
                                                        <c:otherwise>
                                                            <!-- Default case when badge is not 1, 2, or 3 -->
                                                        </c:otherwise>
                                                    </c:choose>
                                                </h5>
                                            </a>
                                            <a class="channel-name-l" href="">
                                                <p class="channel-name"> ${channel.name}</p>
                                            </a>
                                            <input type="hidden" class="channel-link-input" />
                                        </div>
                                    </div>
                                </div>
                                <div class="live-follow-div-mobile">
                                    <button type="button" class="follow-button-mobile">
                                        <span>Follow</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!--Profile stories-->
                        <div class="stories">
                            <div class="stories-video-title">
                                <hr class="hr-line-stories">
                                </hr>
                                <span class="stories-video-title-span">More Posts from this Channel</span>
                                <hr class="hr-line-stories">
                                </hr>
                            </div>
                            <div id="videos-container-mobile">

                            </div>
                            <div class="div-footer-guide-wrapper">
                                <div class="guide-wrapper-footer-div-cover">
                                </div>
                                <div class="div-cta-content-wrapper-footer">
                                    <p class="p-guide-title">Create Your Own Posts</p>
                                    <div class="matrix-smart-wrapper">
                                        <button class="open-mood-app-btn">Open Mood App</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--profile stories ends-->


                    </div>




                    <%@include file="../partials/downloadModalMobile.jsp" %>

                        <div class="mobile-footer">
                            <div class="footer-nav">
                                <a class="for-you-l">
                                    <img src="/assets/img/nav/home1.svg" alt="home">
                                    <p> home </p>
                                </a>
                            </div>
                            <div class="footer-nav">
                                <a class="explore-l">
                                    <img src="/assets/img/nav/friends1.svg" alt="friends">
                                    <p> friends </p>
                                </a>
                            </div>
                            <div class="footer-nav">
                                <a class="live-l">
                                    <img src="/assets/img/nav/live1.svg" alt="live">
                                    <p> live </p>
                                </a>
                            </div>
                            <div class="footer-nav">
                                <a class="account-l">
                                    <img src="/assets/img/nav/you1.svg" alt="user">
                                    <p> user </p>
                                </a>
                            </div>
                        </div>

                        <div class="loader-more-mobile">
                            <lottie-player src="/assets/lottie/moodLoading.json" background="transparent" speed="1"
                                style="width: 150px; height: 150px;" loop autoplay></lottie-player>
                        </div>
                        <!-- <div class="footer"></div> -->
                </div>

                <%@include file="../partials/sharedScripts.jsp" %>
                    <script src="${context}/assets/js/Live/liveStream.js"></script>
                    <script src="${context}/assets/js/config.js"></script>
                    <script src="${context}/assets/js/debug/GenerateTestUserSig.js"></script>
                    <script src="${context}/assets/js/common.js"></script>
                    <script src="${context}/assets/js/device-testing.js"></script>
                    <script src="${context}/assets/js/iconfont.js"></script>
                    <script src="${context}/assets/js/lib-generate-test-usersig.min.js"></script>
                    <script src="${context}/assets/js/presetting.js"></script>
                    <script src="${context}/assets/js/Live/rtc-client-live.js"></script>
                    <script src="${context}/assets/js/rtc-detection.js"></script>
                    <script src="${context}/assets/js/trtc.js"></script>
                    <script src="${context}/assets/js/device-uuid.min.js"></script>
                    <script src="${context}/assets/js/nav.js"></script>
                    <script src="${context}/assets/js/Login/login-primary.js"></script>
                    <script src="${context}/assets/js/Search/navSearch.js"></script>

            </body>

            </html>